import React from 'react'
import Header from './Header'
// import Footer from './Footer'
import Head from 'next/head'
import "../resources/sass/styles.scss"

 class Layout extends React.Component{
  render() {
    const {children} = this.props
    return (
      <>
        <Head>
        <title>BaseApp</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" key="viewport" />
            <link rel="stylesheet" href="/_next/static/style.css" />
        </Head>
        <Header/>
        <main>
            {children}
        </main>

      </>
    )
  }
}


export default Layout