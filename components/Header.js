import React from 'react'
import Link from 'next/link'
 class Header extends React.Component{
  render() {
    return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
            <a className="navbar-item" href="#">
                <img src="/static/assets/img/svg/ThinkTelic-Logo.svg" alt="" width="112" height="50" />
            </a>
        </div>
        <div className="navbar-menu">
            <div className="navbar-end">
            <Link href="/">
                <a  className="navbar-item">About</a>
            </Link>
            </div>
        </div>
      </nav>
    )
  }
}


export default Header 