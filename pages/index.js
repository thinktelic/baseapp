import Link from 'next/link'
import {APP_NAME, COMPANY_EMAIL} from "../config/stringConstant"
import Layout from '../components/Layout';


const Index = () => (
    <Layout>
        <ul>
            <li><Link href="/"><a></a></Link></li>
        </ul>
        <h1>{APP_NAME} BaseApp</h1>
        {COMPANY_EMAIL} 
        <img src="/static/homer-simpson.svg" alt="my image" />
        <i className="fab fa-js"></i>
    </Layout>
)


export default Index
